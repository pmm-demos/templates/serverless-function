## A simple `echo` function to get started with GitLab Serverless.

To invoke the function run:

```
curl --header "Content-Type: application/json" --request POST --data '{"Name": "GitLab"}'  <your serverless function URL>
```

See the [documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/) for more information.